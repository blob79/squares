# For Developers  

## Installation  
#### Cabal Sandbox  
We use cabal sandboxes to keep dependencies from interfering with your global haskell environment. Everything gets installed into `.cabal-sandbox` inside your ez directory:

1. Install Haskell & Cabal  
  * You'll need a newish cabal, so either build it from source:  
```
$ git clone git://github.com/haskell/cabal.git  
$ cd cabal/  
$ cabal install Cabal/ cabal-install/  
```

  * Or use cabal to upgrade itself!  
```
$ cabal update  
$ cabal install cabal  
$ cabal install cabal-install  
```

  * You'll probably need to update your $PATH to point to the new `cabal` executable  
    eg. `PATH=~/.cabal/bin:$PATH`

2. Setup your `squares` cabal sandbox  
```
$ ~/.cabal/bin/cabal sandbox init
```

Build 
```
$ ~/.cabal/bin/cabal install
```

Run it  
```
$ ./.cabal-sandbox/bin/squares
```



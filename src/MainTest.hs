{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}
import qualified Data.Set as Set
import Data.List
import Control.Monad
import Test.SmallCheck
import Test.SmallCheck.Series
import Test.Tasty
import Test.Tasty.SmallCheck
import Test.Tasty.HUnit

data Point = Point { x :: Integer, y :: Integer } deriving(Show,Eq,Ord)
data Edge = Edge { l :: Point , r :: Point } deriving(Show,Eq,Ord)
newtype Square = Square { getTopEdge :: Edge } deriving(Show,Eq,Ord)
squareSize = edgeSize . getTopEdge

-- Return all edges for the given Square
edges :: Square -> [Edge]
edges s = [
    Edge p1 p2,
    Edge p2 p3,
    Edge p3 p4,
    Edge p4 p1
  ]
  where (Edge p1@(Point lx ly) p2@(Point rx ry)) = getTopEdge s
        low = ly + rx - lx
        p3 = Point rx low
        p4 = Point lx low
edgeSize (Edge (Point x _) (Point x2 _)) = x2 - x

toEdges :: [Square] -> Set.Set Edge
toEdges = Set.fromList . concat . (map edges)

-- Check if a square covers another square
covers :: Square -> Square -> Bool
covers large small = let (ss1, ss2) = spanningPoints small in inSpanning large ss1 && inSpanning large ss2

overlaps :: Square -> Square -> Bool
overlaps s1 s2 = fallsInSpanning s1 s2 || fallsInSpanning s2 s1
  where fallsInSpanning ss1 ss2 = any (\e -> inSpanning ss1 (l e)) (edges ss2)





spanningPoints s = let (Edge p1@(Point x1 y1) (Point x2 _)) = getTopEdge s in (p1, (Point x2 (y1 + x2 - x1)))
inSpanning s (Point x y) = let ((Point xs1 ys1), (Point xs2 ys2)) = spanningPoints s in xs1 <= x && x <= xs2 && ys1 <= y && y <= ys2

-- TODO don't have a test here
allInField :: Integer -> [Square]
allInField field = concat [ allInFieldOfSize field s | s <- [1,2..field]]

allInFieldOfSize :: Integer -> Integer -> [Square]
allInFieldOfSize field size = [ Square (Edge (Point x y) (Point (x + size) y)) | x <- [0,1..maxv], y <-[0,1..maxv]]
  where maxv = field - size

-- Draw the given edges for debugging
draw :: [Edge] -> String
draw es = concat [ drawBetween y ++ "\n" ++ drawBelow y ++ "\n" | y <-[0..field]]
  where field = maximum . concat $ map (\(Edge (Point x1 y1) (Point x2 y2)) -> [x1, x2, y1, y2]) es
        drawBetween y = concat ["o" ++ drawEdge "-" x y (x+1) y | x <- [0..field]]
        drawBelow y = concat [drawEdge "|" x (y) x (y+1) ++ " " | x <- [0..field]]
        drawEdge c x1 y1 x2 y2 = if (Edge (Point x1 y1) (Point x2 y2)) `elem` es then c else " "

-- Convert an edge to an edge of size 1
edge1 :: [Edge] -> [Edge]
edge1 es = concat (map pp es)
  where pp (Edge p1 p2) = e1 (min p1 p2) (max p1 p2)
        e1 (Point x1 y1) (Point x2 y2)
          | y1 == y2 = [ Edge (Point i y1) (Point (i + 1) y1) | i <- [x1..(x2 - 1)]]
          | x1 == x2 = [ Edge (Point x1 i) (Point x1 (i + 1)) | i <- [y1..(y2 - 1)]]

chunks2 :: [a] -> [[a]]
chunks2 [] = []
chunks2 s = take 2 s : chunks2 (drop 2 s)

hit :: String -> Bool
hit "o-" = True
hit "| " = True
hit _    = False

parse :: String -> [Edge]
parse s = concat [
  [Edge (Point x y) (Point (x+1) y) | (b,x) <- zip between [0..], b] ++
  [Edge (Point x y) (Point x (y+1)) | (b,x) <- zip below [0..], b] |
  ((between : below : []), y) <- zip (rows s) [0..] ]
  where rows s = chunks2 (map ((map hit) . chunks2) (lines s))

-- TODO isSolution :: [Edges] -> [Square] -> Bool

instance Monad m => Serial m Point where
  series = do
    Positive x <- series
    Positive y <- series
    return $ Point (x-1) (y-1)
instance Monad m => Serial m Edge where
  series = do
    l@(Point lx ly) <- series
    Positive size <- series
    return $ Edge l (Point (lx+size) ly) --TODO have to check invariants for Edge Point sorting
instance Monad m => Serial m Square where
  series = liftM Square series


propTopEdgeInEdges :: Edge -> Bool
propTopEdgeInEdges e =
  (head . edges) (Square e) == e

propEdgesLength :: Square -> Bool
propEdgesLength s =
  (length . edges)  s == 4

pairs :: [a] -> [(a,a)]
pairs as = zip ([e] ++ es) (es ++ [e])
  where e:es = as

propEdgesContinous s = all neib_cont ps
  where ps = pairs . edges $ s
        neib_cont (n1, n2) = r n1 == l n2

propEdgesSameSize s = all samesize es
  where es = edges s
        expected = edgeSize . head $ es
        samesize e = expected == edgeSize e
        edgeSize e =
          let (Edge (Point lx ly) (Point rx ry)) = e
          in abs $ (ry - ly) + (rx - lx)

propToEdges :: [Square] -> Bool
propToEdges ss = let actual = toEdges ss in all (\s -> all (\e -> Set.member e actual) (edges s)) ss


propCoversSelf s = covers s s

-- square of the same size does not cover after moving (TODO could also shrink)
propCoversNotAfterMoving s mx my = mx /= 0 || my /= 0 ==> not (covers moved s) && not (covers s moved)
  where (Edge (Point lx ly) (Point rx _)) = getTopEdge s
        moved = Square $ Edge (Point (lx+mx) (ly+my)) (Point (rx+mx) (ly+my))

-- a bigger square covers when it's moved at most size difference (between the bigger and smaller square)
propCoversByBiggerAfterMoving s (Positive dlx) (Positive dly) (Positive drx) = covers bigger s
  where (Edge (Point lx ly) (Point rx _)) = getTopEdge s
        bigger = Square $ Edge (Point (lx-dlx) (ly-dly)) (Point (rx+dlx+dly+drx) ly)

propOverlapsSelf s = overlaps s s
propOverlapsForCovers s1 s2 = covers s1 s2 ==> overlaps s1 s2 && overlaps s2 s1
-- TODO could also shrink the square
propOverlapsInSquare s mx my = overlaps s moved && overlaps moved s
 where (dx,dy) = (mod mx (squareSize s), mod my (squareSize s))
       (Edge (Point lx ly) (Point rx _)) = getTopEdge s
       moved = Square (Edge (Point (lx + dx) (ly + dy)) (Point (rx + dx) (ly+dy)))

-- TODO could also shrink the square
propOverlapsNotInSquare s (Positive dx) (Positive dy) = not (overlaps s moved) && not (overlaps moved s)
 where (Edge (Point lx ly) (Point rx _)) = getTopEdge s
       size = squareSize s
       moved = Square (Edge (Point (lx + size + dx) (ly + size + dy)) (Point (rx + size + dx) (ly + size + dy)))


propGenerateCardinality (Positive field) (Positive size) =
  field >= size ==> (genericLength . Set.toList . Set.fromList . (allInFieldOfSize field)) size == (field - size + 1) * (field - size + 1)
propGenerateSize (Positive field) (Positive size) =
  field >= size ==> all id [size == squareSize e | e <- allInFieldOfSize field size]
propGenerateInField (Positive field) (Positive size) =
  field >= size ==> all id [covers (Square (Edge (Point 0 0) (Point field 0))) e | e <- allInFieldOfSize field size]

testDrawEdges = expected @=? draw [
    Edge (Point 0 0) (Point 1 0)
    , Edge (Point 0 0) (Point 0 1)
    , Edge (Point 1 0) (Point 1 1)
  ]
  where expected = "o-o \n" ++
                   "| | \n" ++
                   "o o \n" ++
                   "    \n"

propDrawEdges :: Monad m => [Square] -> Property m
propDrawEdges ss = length ss > 0  ==>
  (sort . parse . draw) uniqEdges == uniqEdges
  where uniqEdges = sort . Set.toList . Set.fromList . edge1 . Set.toList . toEdges $ ss

propChunks2Size :: Monad m => String -> Property m
propChunks2Size s = length s > 0 ==> size2 && exceptLast
  where size2 = and $ map (\c -> length c == 2) $ (init . chunks2) s
        exceptLast = (length . last . chunks2) s `elem` [1,2]

propChunks2Concat :: String -> Bool
propChunks2Concat s = s == concat (chunks2 s)
main :: IO ()
main = defaultMain tests


tests =
  testGroup "Squares"
  [
    testProperty "Edges of the square" propTopEdgeInEdges
    , testProperty "Edges length" propEdgesLength
    , testProperty "Edges continous" propEdgesContinous
    , testProperty "Edges size" propEdgesSameSize


    , testProperty "Covers self" propCoversSelf
    , testProperty "Covers moving" propCoversNotAfterMoving
    , testProperty "Covers by bigger" propCoversByBiggerAfterMoving

    , testProperty "Overlaps self" propOverlapsSelf
    , testProperty "Overlaps covers" propOverlapsForCovers
    , testProperty "Overlaps in square" propOverlapsInSquare
    , testProperty "Overlaps not in square" propOverlapsNotInSquare

    , testProperty "allInFieldOfSize card" propGenerateCardinality
    , testProperty "allInFieldOfSize size" propGenerateSize
    , testProperty "allInFieldOfSize in field" propGenerateInField

    , localOption (SmallCheckDepth 3) $
      testGroup "fixed depth" [
        testProperty "ToEdges" propToEdges
        , testProperty "draw/parse" propDrawEdges
      ]

    , testCase "draw" testDrawEdges
    , testProperty "chunk2 size" propChunks2Size
    , testProperty "chunk2 concat" propChunks2Concat

  ]

